# Get AWS LB IP addresses docker container

`docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --tag aws-lb-ips:0.1 .`

`docker run --detach --name aws-ip --env ACCESSKEY="AK" --env SECRETKEY="BX" --env S3BUCKETPATH="name-of-bucket" aws-lb-ips:0.1`

The bash script inside the container get LB names, then get LB DNS names, then dig IPs of DNS and places output JSON file on S3
