FROM alpine:3.12.1

ARG BUILD_ID=1
ARG BUILD_DATE
ARG VERSION=0.0.7
ARG ARGACCESSKEY=
ARG ARGSECRETKEY=
ARG ARGS3BUCKETPATH=
ARG INSTALL_PACKAGES="python3 py3-pip bash curl jq groff less gnupg bind-tools"
ARG BUILD_PACKAGES="build-base git autoconf automake make pkgconf sed"

LABEL maintainer="Rostyslav Malenko<malenko.rostyslav@airslate.com>"
LABEL version="$VERSION"
LABEL description="This is custom Alpine Docker Image for the AWS cli."
LABEL org.label-schema.build-date=$BUILD_DATE

ENV BUILD_ID=${BUILD_ID} \
    APPLICATION_VERSION=${VERSION} \
    SERVICE_ROLE=cron \
    NUMPROCS_WORKER=1 \
    JO_VERSION="1.4" \
    WORKSD="/json" \
    ACCESSKEY=$ARGACCESSKEY \
    SECRETKEY=$ARGSECRETKEY \
    S3BUCKETPATH=$ARGS3BUCKETPATH

COPY ./get_ip_from_aws_lb.sh /json/get_ip_from_aws_lb.sh
COPY --chown=root /.s3cfg /root/.s3cfg
ADD ./aws /root/.aws

RUN apk -v --update add --no-cache $INSTALL_PACKAGES $BUILD_PACKAGES && \
    pip install --upgrade awscli==1.18.185 s3cmd==2.1.0 python-magic==0.4.18 && \
    (crontab -l 2>/dev/null; echo "5 */1 * * * $WORKSD/get_ip_from_aws_lb.sh && s3cmd sync $WORKSD/aws-elb-ip.json --check-md5 --delete-removed --quiet --stop-on-error s3://$S3BUCKETPATH") | crontab - && \
    chmod u+x $WORKSD/get_ip_from_aws_lb.sh && \
    chmod -R 600 /root/.s3cfg /root/.aws && chown -R root:root /root/.aws && \
    git clone --depth 1 --branch $JO_VERSION https://github.com/jpmens/jo.git /tmp/jo && \
    cd /tmp/jo && autoreconf -i && ./configure && make check && make install && \
    apk del $BUILD_PACKAGES && \
    rm -rf /tmp/jo /var/cache/apk/*
    # sed -i -e "s/_ACCESSKEY_/$ACCESSKEY/g" -e "s/_SECRETKEY_/$SECRETKEY/g" /root/.aws/credentials
    # jo -v

WORKDIR /json
CMD sed -i -e "s/_ACCESSKEY_/$ACCESSKEY/g" -e "s/_SECRETKEY_/$SECRETKEY/g" /root/.aws/credentials && crond -f -l 5
