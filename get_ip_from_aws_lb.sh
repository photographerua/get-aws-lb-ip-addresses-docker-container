#!/usr/bin/env bash

awsprofile="as-stage"
workdir="/json"

rm ${workdir}/elb.json
rm ${workdir}/aws-elb-ip.json

aws elbv2 describe-load-balancers --profile=${awsprofile} >> ${workdir}/elb.json

while IFS= read -r lbinternet
do
    lbintdnsname="$(cat ${workdir}/elb.json | jq -r --arg lbinternetarg "${lbinternet}" '.LoadBalancers[] | select(.LoadBalancerName==$lbinternetarg) | .DNSName')"
    # jo -p LBName="${lbinternet}" DNSName="${lbintdnsname}" IP=$(jo -a $(dig +short ${lbintdnsname} A)) >> ${workdir}/aws-elb-ip.json
    jo -p LBName="${lbinternet}" DNSName="${lbintdnsname}" IP=$(jo -a $(dig +short ${lbintdnsname} A)) | jq --arg date "$(date +%s)" '. + {date: $date}' >> ${workdir}/aws-elb-ip.json
done < <(cat ${workdir}/elb.json | jq -r '.LoadBalancers[] | select(.Scheme=="internet-facing") | .LoadBalancerName')
